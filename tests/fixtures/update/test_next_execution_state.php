<?php

/**
 * @file
 * Test fixture for 'next_execution' state API migration.
 */

use Drupal\Core\Database\Database;

$connection = Database::getConnection();

// Set the schema version.
$connection->insert('key_value')
  ->fields([
    'collection' => 'system.schema',
    'name' => 'twitter_entity',
    'value' => 's:4:"8000";',
  ])
  ->execute();

// Update core.extension.
$extensions = $connection->select('config')
  ->fields('config', ['data'])
  ->condition('collection', '')
  ->condition('name', 'core.extension')
  ->execute()
  ->fetchField();
$extensions = unserialize($extensions);
$extensions['module']['twitter_entity'] = 0;
$connection->update('config')
  ->fields(['data' => serialize($extensions)])
  ->condition('collection', '')
  ->condition('name', 'core.extension')
  ->execute();

// Set outdated module config.
$connection->insert('config')
  ->fields([
    'collection' => '',
    'name' => 'twitter_entity.settings',
    'data' => serialize([
      'tweets_number_per_request' => 5,
      'fetch_interval' => 3600,
      'next_execution' => 2500,
    ]),
  ])
  ->execute();

// Add twitter_entity entity.
$connection->insert('key_value')
  ->fields([
    'collection',
    'name',
    'value',
  ])
  ->values([
    'collection' => 'entity.definitions.installed',
    'name' => 'twitter_entity.entity_type',
    'value' => 'O:36:"Drupal\Core\Entity\ContentEntityType":42:{s:25:" * revision_metadata_keys";a:1:{s:16:"revision_default";s:16:"revision_default";}s:31:" * requiredRevisionMetadataKeys";a:1:{s:16:"revision_default";s:16:"revision_default";}s:15:" * static_cache";b:1;s:15:" * render_cache";b:1;s:19:" * persistent_cache";b:1;s:14:" * entity_keys";a:9:{s:2:"id";s:2:"id";s:4:"uuid";s:4:"uuid";s:8:"langcode";s:8:"langcode";s:6:"status";s:6:"status";s:7:"created";s:7:"created";s:8:"revision";s:0:"";s:6:"bundle";s:0:"";s:16:"default_langcode";s:16:"default_langcode";s:29:"revision_translation_affected";s:29:"revision_translation_affected";}s:5:" * id";s:14:"twitter_entity";s:16:" * originalClass";s:42:"Drupal\twitter_entity\Entity\TwitterEntity";s:11:" * handlers";a:7:{s:12:"view_builder";s:36:"Drupal\Core\Entity\EntityViewBuilder";s:12:"list_builder";s:46:"Drupal\twitter_entity\TwitterEntityListBuilder";s:10:"views_data";s:51:"Drupal\twitter_entity\Entity\TwitterEntityViewsData";s:4:"form";a:3:{s:7:"default";s:44:"Drupal\twitter_entity\Form\TwitterEntityForm";s:4:"edit";s:44:"Drupal\twitter_entity\Form\TwitterEntityForm";s:6:"delete";s:50:"Drupal\twitter_entity\Form\TwitterEntityDeleteForm";}s:6:"access";s:55:"Drupal\twitter_entity\TwitterEntityAccessControlHandler";s:14:"route_provider";a:1:{s:4:"html";s:52:"Drupal\twitter_entity\TwitterEntityHtmlRouteProvider";}s:7:"storage";s:46:"Drupal\Core\Entity\Sql\SqlContentEntityStorage";}s:19:" * admin_permission";s:27:"administer twitter entities";s:25:" * permission_granularity";s:11:"entity_type";s:8:" * links";a:3:{s:9:"edit-form";s:42:"/admin/content/tweet/{twitter_entity}/edit";s:11:"delete-form";s:44:"/admin/content/tweet/{twitter_entity}/delete";s:10:"collection";s:20:"/admin/content/tweet";}s:17:" * label_callback";N;s:21:" * bundle_entity_type";N;s:12:" * bundle_of";N;s:15:" * bundle_label";N;s:13:" * base_table";s:14:"twitter_entity";s:22:" * revision_data_table";N;s:17:" * revision_table";N;s:13:" * data_table";N;s:11:" * internal";b:0;s:15:" * translatable";b:0;s:19:" * show_revision_ui";b:0;s:8:" * label";O:48:"Drupal\Core\StringTranslation\TranslatableMarkup":3:{s:9:" * string";s:14:"Twitter entity";s:12:" * arguments";a:0:{}s:10:" * options";a:0:{}}s:19:" * label_collection";s:0:"";s:17:" * label_singular";s:0:"";s:15:" * label_plural";s:0:"";s:14:" * label_count";a:0:{}s:15:" * uri_callback";N;s:8:" * group";s:7:"content";s:14:" * group_label";O:48:"Drupal\Core\StringTranslation\TranslatableMarkup":3:{s:9:" * string";s:7:"Content";s:12:" * arguments";a:0:{}s:10:" * options";a:1:{s:7:"context";s:17:"Entity type group";}}s:22:" * field_ui_base_route";N;s:26:" * common_reference_target";b:0;s:22:" * list_cache_contexts";a:0:{}s:18:" * list_cache_tags";a:1:{i:0;s:19:"twitter_entity_list";}s:14:" * constraints";a:2:{s:13:"EntityChanged";N;s:26:"EntityUntranslatableFields";N;}s:13:" * additional";a:0:{}s:8:" * class";s:42:"Drupal\twitter_entity\Entity\TwitterEntity";s:11:" * provider";s:14:"twitter_entity";s:14:" * _serviceIds";a:0:{}s:18:" * _entityStorages";a:0:{}s:20:" * stringTranslation";N;}',
  ])
  ->values([
    'collection' => 'entity.definitions.installed',
    'name' => 'twitter_entity.field_storage_definitions',
    'value' => 'a:11:{s:2:"id";O:37:"Drupal\Core\Field\BaseFieldDefinition":5:{s:7:" * type";s:7:"integer";s:9:" * schema";a:4:{s:7:"columns";a:1:{s:5:"value";a:3:{s:4:"type";s:3:"int";s:8:"unsigned";b:1;s:4:"size";s:6:"normal";}}s:11:"unique keys";a:0:{}s:7:"indexes";a:0:{}s:12:"foreign keys";a:0:{}}s:10:" * indexes";a:0:{}s:17:" * itemDefinition";O:51:"Drupal\Core\Field\TypedData\FieldItemDataDefinition":2:{s:18:" * fieldDefinition";r:2;s:13:" * definition";a:2:{s:4:"type";s:18:"field_item:integer";s:8:"settings";a:6:{s:8:"unsigned";b:1;s:4:"size";s:6:"normal";s:3:"min";s:0:"";s:3:"max";s:0:"";s:6:"prefix";s:0:"";s:6:"suffix";s:0:"";}}}s:13:" * definition";a:7:{s:5:"label";O:48:"Drupal\Core\StringTranslation\TranslatableMarkup":3:{s:9:" * string";s:2:"ID";s:12:" * arguments";a:0:{}s:10:" * options";a:0:{}}s:9:"read-only";b:1;s:8:"provider";s:14:"twitter_entity";s:10:"field_name";s:2:"id";s:11:"entity_type";s:14:"twitter_entity";s:6:"bundle";N;s:13:"initial_value";N;}}s:4:"uuid";O:37:"Drupal\Core\Field\BaseFieldDefinition":5:{s:7:" * type";s:4:"uuid";s:9:" * schema";a:4:{s:7:"columns";a:1:{s:5:"value";a:3:{s:4:"type";s:13:"varchar_ascii";s:6:"length";i:128;s:6:"binary";b:0;}}s:11:"unique keys";a:1:{s:5:"value";a:1:{i:0;s:5:"value";}}s:7:"indexes";a:0:{}s:12:"foreign keys";a:0:{}}s:10:" * indexes";a:0:{}s:17:" * itemDefinition";O:51:"Drupal\Core\Field\TypedData\FieldItemDataDefinition":2:{s:18:" * fieldDefinition";r:36;s:13:" * definition";a:2:{s:4:"type";s:15:"field_item:uuid";s:8:"settings";a:3:{s:10:"max_length";i:128;s:8:"is_ascii";b:1;s:14:"case_sensitive";b:0;}}}s:13:" * definition";a:7:{s:5:"label";O:48:"Drupal\Core\StringTranslation\TranslatableMarkup":3:{s:9:" * string";s:4:"UUID";s:12:" * arguments";a:0:{}s:10:" * options";a:0:{}}s:9:"read-only";b:1;s:8:"provider";s:14:"twitter_entity";s:10:"field_name";s:4:"uuid";s:11:"entity_type";s:14:"twitter_entity";s:6:"bundle";N;s:13:"initial_value";N;}}s:8:"langcode";O:37:"Drupal\Core\Field\BaseFieldDefinition":5:{s:7:" * type";s:8:"language";s:9:" * schema";a:4:{s:7:"columns";a:1:{s:5:"value";a:2:{s:4:"type";s:13:"varchar_ascii";s:6:"length";i:12;}}s:11:"unique keys";a:0:{}s:7:"indexes";a:0:{}s:12:"foreign keys";a:0:{}}s:10:" * indexes";a:0:{}s:17:" * itemDefinition";O:51:"Drupal\Core\Field\TypedData\FieldItemDataDefinition":2:{s:18:" * fieldDefinition";r:69;s:13:" * definition";a:2:{s:4:"type";s:19:"field_item:language";s:8:"settings";a:0:{}}}s:13:" * definition";a:7:{s:5:"label";O:48:"Drupal\Core\StringTranslation\TranslatableMarkup":3:{s:9:" * string";s:8:"Language";s:12:" * arguments";a:0:{}s:10:" * options";a:0:{}}s:7:"display";a:2:{s:4:"view";a:1:{s:7:"options";a:1:{s:6:"region";s:6:"hidden";}}s:4:"form";a:1:{s:7:"options";a:2:{s:4:"type";s:15:"language_select";s:6:"weight";i:2;}}}s:8:"provider";s:14:"twitter_entity";s:10:"field_name";s:8:"langcode";s:11:"entity_type";s:14:"twitter_entity";s:6:"bundle";N;s:13:"initial_value";N;}}s:6:"status";O:37:"Drupal\Core\Field\BaseFieldDefinition":5:{s:7:" * type";s:7:"boolean";s:9:" * schema";a:4:{s:7:"columns";a:1:{s:5:"value";a:2:{s:4:"type";s:3:"int";s:4:"size";s:4:"tiny";}}s:11:"unique keys";a:0:{}s:7:"indexes";a:0:{}s:12:"foreign keys";a:0:{}}s:10:" * indexes";a:0:{}s:17:" * itemDefinition";O:51:"Drupal\Core\Field\TypedData\FieldItemDataDefinition":2:{s:18:" * fieldDefinition";r:103;s:13:" * definition";a:2:{s:4:"type";s:18:"field_item:boolean";s:8:"settings";a:2:{s:8:"on_label";O:48:"Drupal\Core\StringTranslation\TranslatableMarkup":3:{s:9:" * string";s:2:"On";s:12:" * arguments";a:0:{}s:10:" * options";a:0:{}}s:9:"off_label";O:48:"Drupal\Core\StringTranslation\TranslatableMarkup":3:{s:9:" * string";s:3:"Off";s:12:" * arguments";a:0:{}s:10:" * options";a:0:{}}}}}s:13:" * definition";a:9:{s:5:"label";O:48:"Drupal\Core\StringTranslation\TranslatableMarkup":3:{s:9:" * string";s:17:"Publishing status";s:12:" * arguments";a:0:{}s:10:" * options";a:0:{}}s:11:"description";O:48:"Drupal\Core\StringTranslation\TranslatableMarkup":3:{s:9:" * string";s:52:"A boolean indicating whether the Tweet is published.";s:12:" * arguments";a:0:{}s:10:" * options";a:0:{}}s:7:"display";a:1:{s:4:"form";a:1:{s:7:"options";a:2:{s:4:"type";s:7:"boolean";s:6:"weight";i:1;}}}s:13:"default_value";a:1:{i:0;a:1:{s:5:"value";b:1;}}s:8:"provider";s:14:"twitter_entity";s:10:"field_name";s:6:"status";s:11:"entity_type";s:14:"twitter_entity";s:6:"bundle";N;s:13:"initial_value";N;}}s:7:"created";O:37:"Drupal\Core\Field\BaseFieldDefinition":5:{s:7:" * type";s:7:"created";s:9:" * schema";a:4:{s:7:"columns";a:1:{s:5:"value";a:1:{s:4:"type";s:3:"int";}}s:11:"unique keys";a:0:{}s:7:"indexes";a:0:{}s:12:"foreign keys";a:0:{}}s:10:" * indexes";a:0:{}s:17:" * itemDefinition";O:51:"Drupal\Core\Field\TypedData\FieldItemDataDefinition":2:{s:18:" * fieldDefinition";r:149;s:13:" * definition";a:2:{s:4:"type";s:18:"field_item:created";s:8:"settings";a:0:{}}}s:13:" * definition";a:8:{s:5:"label";O:48:"Drupal\Core\StringTranslation\TranslatableMarkup":3:{s:9:" * string";s:7:"Created";s:12:" * arguments";a:0:{}s:10:" * options";a:0:{}}s:11:"description";O:48:"Drupal\Core\StringTranslation\TranslatableMarkup":3:{s:9:" * string";s:37:"The time that the entity was created.";s:12:" * arguments";a:0:{}s:10:" * options";a:0:{}}s:7:"display";a:1:{s:4:"form";a:1:{s:7:"options";a:2:{s:4:"type";s:18:"datetime_timestamp";s:6:"weight";i:2;}}}s:8:"provider";s:14:"twitter_entity";s:10:"field_name";s:7:"created";s:11:"entity_type";s:14:"twitter_entity";s:6:"bundle";N;s:13:"initial_value";N;}}s:7:"changed";O:37:"Drupal\Core\Field\BaseFieldDefinition":5:{s:7:" * type";s:7:"changed";s:9:" * schema";a:4:{s:7:"columns";a:1:{s:5:"value";a:1:{s:4:"type";s:3:"int";}}s:11:"unique keys";a:0:{}s:7:"indexes";a:0:{}s:12:"foreign keys";a:0:{}}s:10:" * indexes";a:0:{}s:17:" * itemDefinition";O:51:"Drupal\Core\Field\TypedData\FieldItemDataDefinition":2:{s:18:" * fieldDefinition";r:183;s:13:" * definition";a:2:{s:4:"type";s:18:"field_item:changed";s:8:"settings";a:0:{}}}s:13:" * definition";a:7:{s:5:"label";O:48:"Drupal\Core\StringTranslation\TranslatableMarkup":3:{s:9:" * string";s:7:"Changed";s:12:" * arguments";a:0:{}s:10:" * options";a:0:{}}s:11:"description";O:48:"Drupal\Core\StringTranslation\TranslatableMarkup":3:{s:9:" * string";s:41:"The time that the entity was last edited.";s:12:" * arguments";a:0:{}s:10:" * options";a:0:{}}s:8:"provider";s:14:"twitter_entity";s:10:"field_name";s:7:"changed";s:11:"entity_type";s:14:"twitter_entity";s:6:"bundle";N;s:13:"initial_value";N;}}s:8:"tweet_id";O:37:"Drupal\Core\Field\BaseFieldDefinition":5:{s:7:" * type";s:6:"string";s:9:" * schema";a:4:{s:7:"columns";a:1:{s:5:"value";a:3:{s:4:"type";s:7:"varchar";s:6:"length";i:255;s:6:"binary";b:0;}}s:11:"unique keys";a:0:{}s:7:"indexes";a:0:{}s:12:"foreign keys";a:0:{}}s:10:" * indexes";a:0:{}s:17:" * itemDefinition";O:51:"Drupal\Core\Field\TypedData\FieldItemDataDefinition":2:{s:18:" * fieldDefinition";r:212;s:13:" * definition";a:2:{s:4:"type";s:17:"field_item:string";s:8:"settings";a:3:{s:10:"max_length";i:255;s:8:"is_ascii";b:0;s:14:"case_sensitive";b:0;}}}s:13:" * definition";a:7:{s:5:"label";O:48:"Drupal\Core\StringTranslation\TranslatableMarkup":3:{s:9:" * string";s:8:"Tweet id";s:12:" * arguments";a:0:{}s:10:" * options";a:0:{}}s:11:"description";O:48:"Drupal\Core\StringTranslation\TranslatableMarkup":3:{s:9:" * string";s:33:"Tweet id provided by Twitter API.";s:12:" * arguments";a:0:{}s:10:" * options";a:0:{}}s:8:"provider";s:14:"twitter_entity";s:10:"field_name";s:8:"tweet_id";s:11:"entity_type";s:14:"twitter_entity";s:6:"bundle";N;s:13:"initial_value";N;}}s:11:"tweet_media";O:37:"Drupal\Core\Field\BaseFieldDefinition":5:{s:7:" * type";s:6:"string";s:9:" * schema";a:4:{s:7:"columns";a:1:{s:5:"value";a:3:{s:4:"type";s:7:"varchar";s:6:"length";i:2000;s:6:"binary";b:0;}}s:11:"unique keys";a:0:{}s:7:"indexes";a:0:{}s:12:"foreign keys";a:0:{}}s:10:" * indexes";a:0:{}s:17:" * itemDefinition";O:51:"Drupal\Core\Field\TypedData\FieldItemDataDefinition":2:{s:18:" * fieldDefinition";r:246;s:13:" * definition";a:2:{s:4:"type";s:17:"field_item:string";s:8:"settings";a:3:{s:10:"max_length";i:2000;s:8:"is_ascii";b:0;s:14:"case_sensitive";b:0;}}}s:13:" * definition";a:7:{s:5:"label";O:48:"Drupal\Core\StringTranslation\TranslatableMarkup":3:{s:9:" * string";s:11:"Tweet media";s:12:" * arguments";a:0:{}s:10:" * options";a:0:{}}s:11:"description";O:48:"Drupal\Core\StringTranslation\TranslatableMarkup":3:{s:9:" * string";s:16:"Tweet media url.";s:12:" * arguments";a:0:{}s:10:" * options";a:0:{}}s:8:"provider";s:14:"twitter_entity";s:10:"field_name";s:11:"tweet_media";s:11:"entity_type";s:14:"twitter_entity";s:6:"bundle";N;s:13:"initial_value";N;}}s:10:"tweet_text";O:37:"Drupal\Core\Field\BaseFieldDefinition":5:{s:7:" * type";s:9:"text_long";s:9:" * schema";a:4:{s:7:"columns";a:2:{s:5:"value";a:2:{s:4:"type";s:4:"text";s:4:"size";s:3:"big";}s:6:"format";a:2:{s:4:"type";s:13:"varchar_ascii";s:6:"length";i:255;}}s:7:"indexes";a:1:{s:6:"format";a:1:{i:0;s:6:"format";}}s:11:"unique keys";a:0:{}s:12:"foreign keys";a:0:{}}s:10:" * indexes";a:0:{}s:17:" * itemDefinition";O:51:"Drupal\Core\Field\TypedData\FieldItemDataDefinition":2:{s:18:" * fieldDefinition";r:280;s:13:" * definition";a:2:{s:4:"type";s:20:"field_item:text_long";s:8:"settings";a:0:{}}}s:13:" * definition";a:9:{s:5:"label";O:48:"Drupal\Core\StringTranslation\TranslatableMarkup":3:{s:9:" * string";s:10:"Tweet text";s:12:" * arguments";a:0:{}s:10:" * options";a:0:{}}s:11:"description";O:48:"Drupal\Core\StringTranslation\TranslatableMarkup":3:{s:9:" * string";s:11:"Tweet text.";s:12:" * arguments";a:0:{}s:10:" * options";a:0:{}}s:11:"cardinality";i:1;s:7:"display";a:1:{s:4:"form";a:1:{s:7:"options";a:2:{s:4:"type";s:9:"text_long";s:6:"weight";i:0;}}}s:8:"provider";s:14:"twitter_entity";s:10:"field_name";s:10:"tweet_text";s:11:"entity_type";s:14:"twitter_entity";s:6:"bundle";N;s:13:"initial_value";N;}}s:12:"twitter_user";O:37:"Drupal\Core\Field\BaseFieldDefinition":5:{s:7:" * type";s:6:"string";s:9:" * schema";a:4:{s:7:"columns";a:1:{s:5:"value";a:3:{s:4:"type";s:7:"varchar";s:6:"length";i:255;s:6:"binary";b:0;}}s:11:"unique keys";a:0:{}s:7:"indexes";a:0:{}s:12:"foreign keys";a:0:{}}s:10:" * indexes";a:0:{}s:17:" * itemDefinition";O:51:"Drupal\Core\Field\TypedData\FieldItemDataDefinition":2:{s:18:" * fieldDefinition";r:321;s:13:" * definition";a:2:{s:4:"type";s:17:"field_item:string";s:8:"settings";a:3:{s:10:"max_length";i:255;s:8:"is_ascii";b:0;s:14:"case_sensitive";b:0;}}}s:13:" * definition";a:7:{s:5:"label";O:48:"Drupal\Core\StringTranslation\TranslatableMarkup":3:{s:9:" * string";s:12:"Twitter user";s:12:" * arguments";a:0:{}s:10:" * options";a:0:{}}s:11:"description";O:48:"Drupal\Core\StringTranslation\TranslatableMarkup":3:{s:9:" * string";s:51:"Twitter user name from witch Tweet was pulled from.";s:12:" * arguments";a:0:{}s:10:" * options";a:0:{}}s:8:"provider";s:14:"twitter_entity";s:10:"field_name";s:12:"twitter_user";s:11:"entity_type";s:14:"twitter_entity";s:6:"bundle";N;s:13:"initial_value";N;}}s:13:"full_response";O:37:"Drupal\Core\Field\BaseFieldDefinition":5:{s:7:" * type";s:11:"string_long";s:9:" * schema";a:4:{s:7:"columns";a:1:{s:5:"value";a:2:{s:4:"type";s:4:"text";s:4:"size";s:3:"big";}}s:11:"unique keys";a:0:{}s:7:"indexes";a:0:{}s:12:"foreign keys";a:0:{}}s:10:" * indexes";a:0:{}s:17:" * itemDefinition";O:51:"Drupal\Core\Field\TypedData\FieldItemDataDefinition":2:{s:18:" * fieldDefinition";r:355;s:13:" * definition";a:2:{s:4:"type";s:22:"field_item:string_long";s:8:"settings";a:2:{s:14:"case_sensitive";b:0;s:13:"default_value";s:0:"";}}}s:13:" * definition";a:7:{s:5:"label";O:48:"Drupal\Core\StringTranslation\TranslatableMarkup":3:{s:9:" * string";s:13:"JSON response";s:12:" * arguments";a:0:{}s:10:" * options";a:0:{}}s:11:"description";O:48:"Drupal\Core\StringTranslation\TranslatableMarkup":3:{s:9:" * string";s:36:"Full JSON response from Twitter API.";s:12:" * arguments";a:0:{}s:10:" * options";a:0:{}}s:8:"provider";s:14:"twitter_entity";s:10:"field_name";s:13:"full_response";s:11:"entity_type";s:14:"twitter_entity";s:6:"bundle";N;s:13:"initial_value";N;}}}',
  ])
  ->values([
    'collection' => 'entity.storage_schema.sql',
    'name' => 'twitter_entity.entity_schema_data',
    'value' => 'a:1:{s:14:"twitter_entity";a:1:{s:11:"primary key";a:1:{i:0;s:2:"id";}}}',
  ])
  ->values([
    'collection' => 'entity.storage_schema.sql',
    'name' => 'twitter_entity.field_schema_data.changed',
    'value' => 'a:1:{s:14:"twitter_entity";a:1:{s:6:"fields";a:1:{s:7:"changed";a:2:{s:4:"type";s:3:"int";s:8:"not null";b:0;}}}}',
  ])
  ->values([
    'collection' => 'entity.storage_schema.sql',
    'name' => 'twitter_entity.field_schema_data.created',
    'value' => 'a:1:{s:14:"twitter_entity";a:1:{s:6:"fields";a:1:{s:7:"created";a:2:{s:4:"type";s:3:"int";s:8:"not null";b:1;}}}}',
  ])
  ->values([
    'collection' => 'entity.storage_schema.sql',
    'name' => 'twitter_entity.field_schema_data.full_response',
    'value' => 'a:1:{s:14:"twitter_entity";a:1:{s:6:"fields";a:1:{s:13:"full_response";a:3:{s:4:"type";s:4:"text";s:4:"size";s:3:"big";s:8:"not null";b:0;}}}}',
  ])
  ->values([
    'collection' => 'entity.storage_schema.sql',
    'name' => 'twitter_entity.field_schema_data.id',
    'value' => 'a:1:{s:14:"twitter_entity";a:1:{s:6:"fields";a:1:{s:2:"id";a:4:{s:4:"type";s:3:"int";s:8:"unsigned";b:1;s:4:"size";s:6:"normal";s:8:"not null";b:1;}}}}',
  ])
  ->values([
    'collection' => 'entity.storage_schema.sql',
    'name' => 'twitter_entity.field_schema_data.langcode',
    'value' => 'a:1:{s:14:"twitter_entity";a:1:{s:6:"fields";a:1:{s:8:"langcode";a:3:{s:4:"type";s:13:"varchar_ascii";s:6:"length";i:12;s:8:"not null";b:1;}}}}',
  ])
  ->values([
    'collection' => 'entity.storage_schema.sql',
    'name' => 'twitter_entity.field_schema_data.status',
    'value' => 'a:1:{s:14:"twitter_entity";a:1:{s:6:"fields";a:1:{s:6:"status";a:3:{s:4:"type";s:3:"int";s:4:"size";s:4:"tiny";s:8:"not null";b:1;}}}}',
  ])
  ->values([
    'collection' => 'entity.storage_schema.sql',
    'name' => 'twitter_entity.field_schema_data.tweet_id',
    'value' => 'a:1:{s:14:"twitter_entity";a:1:{s:6:"fields";a:1:{s:8:"tweet_id";a:4:{s:4:"type";s:7:"varchar";s:6:"length";i:255;s:6:"binary";b:0;s:8:"not null";b:0;}}}}',
  ])
  ->values([
    'collection' => 'entity.storage_schema.sql',
    'name' => 'twitter_entity.field_schema_data.tweet_media',
    'value' => 'a:1:{s:14:"twitter_entity";a:1:{s:6:"fields";a:1:{s:11:"tweet_media";a:4:{s:4:"type";s:7:"varchar";s:6:"length";i:2000;s:6:"binary";b:0;s:8:"not null";b:0;}}}}',
  ])
  ->values([
    'collection' => 'entity.storage_schema.sql',
    'name' => 'twitter_entity.field_schema_data.tweet_text',
    'value' => 'a:1:{s:14:"twitter_entity";a:2:{s:6:"fields";a:2:{s:17:"tweet_text__value";a:3:{s:4:"type";s:4:"text";s:4:"size";s:3:"big";s:8:"not null";b:0;}s:18:"tweet_text__format";a:3:{s:4:"type";s:13:"varchar_ascii";s:6:"length";i:255;s:8:"not null";b:0;}}s:7:"indexes";a:1:{s:40:"twitter_entity_field__tweet_text__format";a:1:{i:0;s:18:"tweet_text__format";}}}}',
  ])
  ->values([
    'collection' => 'entity.storage_schema.sql',
    'name' => 'twitter_entity.field_schema_data.twitter_user',
    'value' => 'a:1:{s:14:"twitter_entity";a:1:{s:6:"fields";a:1:{s:12:"twitter_user";a:4:{s:4:"type";s:7:"varchar";s:6:"length";i:255;s:6:"binary";b:0;s:8:"not null";b:0;}}}}',
  ])
  ->values([
    'collection' => 'entity.storage_schema.sql',
    'name' => 'twitter_entity.field_schema_data.uuid',
    'value' => 'a:1:{s:14:"twitter_entity";a:2:{s:6:"fields";a:1:{s:4:"uuid";a:4:{s:4:"type";s:13:"varchar_ascii";s:6:"length";i:128;s:6:"binary";b:0;s:8:"not null";b:1;}}s:11:"unique keys";a:1:{s:33:"twitter_entity_field__uuid__value";a:1:{i:0;s:4:"uuid";}}}}',
  ])
  ->execute();

$connection->insert('router')
  ->fields([
    'name',
    'path',
    'pattern_outline',
    'fit',
    'route',
    'number_parts',
  ])
  ->values([
    'name' => 'entity.twitter_entity.collection',
    'path' => '/admin/content/tweet',
    'pattern_outline' => '/admin/content/tweet',
    'fit' => '7',
    'route' => 'C:31:"Symfony\Component\Routing\Route":924:{a:9:{s:4:"path";s:20:"/admin/content/tweet";s:4:"host";s:0:"";s:8:"defaults";a:2:{s:12:"_entity_list";s:14:"twitter_entity";s:6:"_title";s:6:"Tweets";}s:12:"requirements";a:1:{s:11:"_permission";s:23:"access twitter overview";}s:7:"options";a:4:{s:14:"compiler_class";s:33:"Drupal\Core\Routing\RouteCompiler";s:12:"_admin_route";b:1;s:14:"_access_checks";a:1:{i:0;s:23:"access_check.permission";}s:4:"utf8";b:1;}s:7:"schemes";a:0:{}s:7:"methods";a:2:{i:0;s:3:"GET";i:1;s:4:"POST";}s:9:"condition";s:0:"";s:8:"compiled";C:33:"Drupal\Core\Routing\CompiledRoute":357:{a:11:{s:4:"vars";a:0:{}s:11:"path_prefix";s:0:"";s:10:"path_regex";s:27:"#^/admin/content/tweet$#sDu";s:11:"path_tokens";a:1:{i:0;a:2:{i:0;s:4:"text";i:1;s:20:"/admin/content/tweet";}}s:9:"path_vars";a:0:{}s:10:"host_regex";N;s:11:"host_tokens";a:0:{}s:9:"host_vars";a:0:{}s:3:"fit";i:7;s:14:"patternOutline";s:20:"/admin/content/tweet";s:8:"numParts";i:3;}}}}',
    'number_parts' => '3',
  ])
  ->values([
    'name' => 'entity.twitter_entity.delete_form',
    'path' => '/admin/content/tweet/{twitter_entity}/delete',
    'pattern_outline' => '/admin/content/tweet/%/delete',
    'fit' => '29',
    'route' => 'C:31:"Symfony\Component\Routing\Route":1411:{a:9:{s:4:"path";s:44:"/admin/content/tweet/{twitter_entity}/delete";s:4:"host";s:0:"";s:8:"defaults";a:2:{s:12:"_entity_form";s:21:"twitter_entity.delete";s:15:"_title_callback";s:60:"\Drupal\Core\Entity\Controller\EntityController::deleteTitle";}s:12:"requirements";a:2:{s:14:"_entity_access";s:21:"twitter_entity.delete";s:14:"twitter_entity";s:3:"\d+";}s:7:"options";a:5:{s:14:"compiler_class";s:33:"Drupal\Core\Routing\RouteCompiler";s:10:"parameters";a:1:{s:14:"twitter_entity";a:2:{s:4:"type";s:21:"entity:twitter_entity";s:9:"converter";s:21:"paramconverter.entity";}}s:12:"_admin_route";b:1;s:14:"_access_checks";a:1:{i:0;s:19:"access_check.entity";}s:4:"utf8";b:1;}s:7:"schemes";a:0:{}s:7:"methods";a:2:{i:0;s:3:"GET";i:1;s:4:"POST";}s:9:"condition";s:0:"";s:8:"compiled";C:33:"Drupal\Core\Routing\CompiledRoute":582:{a:11:{s:4:"vars";a:1:{i:0;s:14:"twitter_entity";}s:11:"path_prefix";s:0:"";s:10:"path_regex";s:58:"#^/admin/content/tweet/(?P<twitter_entity>\d+)/delete$#sDu";s:11:"path_tokens";a:3:{i:0;a:2:{i:0;s:4:"text";i:1;s:7:"/delete";}i:1;a:5:{i:0;s:8:"variable";i:1;s:1:"/";i:2;s:3:"\d+";i:3;s:14:"twitter_entity";i:4;b:1;}i:2;a:2:{i:0;s:4:"text";i:1;s:20:"/admin/content/tweet";}}s:9:"path_vars";a:1:{i:0;s:14:"twitter_entity";}s:10:"host_regex";N;s:11:"host_tokens";a:0:{}s:9:"host_vars";a:0:{}s:3:"fit";i:29;s:14:"patternOutline";s:29:"/admin/content/tweet/%/delete";s:8:"numParts";i:5;}}}}',
    'number_parts' => '5',
  ])
  ->values([
    'name' => 'entity.twitter_entity.edit_form',
    'path' => '/admin/content/tweet/{twitter_entity}/edit',
    'pattern_outline' => '/admin/content/tweet/%/edit',
    'fit' => '29',
    'route' => 'C:31:"Symfony\Component\Routing\Route":1399:{a:9:{s:4:"path";s:42:"/admin/content/tweet/{twitter_entity}/edit";s:4:"host";s:0:"";s:8:"defaults";a:2:{s:12:"_entity_form";s:19:"twitter_entity.edit";s:15:"_title_callback";s:58:"\Drupal\Core\Entity\Controller\EntityController::editTitle";}s:12:"requirements";a:2:{s:14:"_entity_access";s:21:"twitter_entity.update";s:14:"twitter_entity";s:3:"\d+";}s:7:"options";a:5:{s:14:"compiler_class";s:33:"Drupal\Core\Routing\RouteCompiler";s:10:"parameters";a:1:{s:14:"twitter_entity";a:2:{s:4:"type";s:21:"entity:twitter_entity";s:9:"converter";s:21:"paramconverter.entity";}}s:12:"_admin_route";b:1;s:14:"_access_checks";a:1:{i:0;s:19:"access_check.entity";}s:4:"utf8";b:1;}s:7:"schemes";a:0:{}s:7:"methods";a:2:{i:0;s:3:"GET";i:1;s:4:"POST";}s:9:"condition";s:0:"";s:8:"compiled";C:33:"Drupal\Core\Routing\CompiledRoute":576:{a:11:{s:4:"vars";a:1:{i:0;s:14:"twitter_entity";}s:11:"path_prefix";s:0:"";s:10:"path_regex";s:56:"#^/admin/content/tweet/(?P<twitter_entity>\d+)/edit$#sDu";s:11:"path_tokens";a:3:{i:0;a:2:{i:0;s:4:"text";i:1;s:5:"/edit";}i:1;a:5:{i:0;s:8:"variable";i:1;s:1:"/";i:2;s:3:"\d+";i:3;s:14:"twitter_entity";i:4;b:1;}i:2;a:2:{i:0;s:4:"text";i:1;s:20:"/admin/content/tweet";}}s:9:"path_vars";a:1:{i:0;s:14:"twitter_entity";}s:10:"host_regex";N;s:11:"host_tokens";a:0:{}s:9:"host_vars";a:0:{}s:3:"fit";i:29;s:14:"patternOutline";s:27:"/admin/content/tweet/%/edit";s:8:"numParts";i:5;}}}}',
    'number_parts' => '5',
  ])
  ->values([
    'name' => 'twitter_entity.manual_pull',
    'path' => '/admin/config/services/twitter-entity/manual-pull',
    'pattern_outline' => '/admin/config/services/twitter-entity/manual-pull',
    'fit' => '31',
    'route' => 'C:31:"Symfony\Component\Routing\Route":1113:{a:9:{s:4:"path";s:49:"/admin/config/services/twitter-entity/manual-pull";s:4:"host";s:0:"";s:8:"defaults";a:2:{s:11:"_controller";s:67:"\Drupal\twitter_entity\Controller\TwitterPageController::manualPull";s:6:"_title";s:19:"Twitter manual pull";}s:12:"requirements";a:1:{s:11:"_permission";s:27:"administer twitter entities";}s:7:"options";a:4:{s:14:"compiler_class";s:33:"Drupal\Core\Routing\RouteCompiler";s:4:"utf8";b:1;s:12:"_admin_route";b:1;s:14:"_access_checks";a:1:{i:0;s:23:"access_check.permission";}}s:7:"schemes";a:0:{}s:7:"methods";a:2:{i:0;s:3:"GET";i:1;s:4:"POST";}s:9:"condition";s:0:"";s:8:"compiled";C:33:"Drupal\Core\Routing\CompiledRoute":447:{a:11:{s:4:"vars";a:0:{}s:11:"path_prefix";s:0:"";s:10:"path_regex";s:58:"#^/admin/config/services/twitter\-entity/manual\-pull$#sDu";s:11:"path_tokens";a:1:{i:0;a:2:{i:0;s:4:"text";i:1;s:49:"/admin/config/services/twitter-entity/manual-pull";}}s:9:"path_vars";a:0:{}s:10:"host_regex";N;s:11:"host_tokens";a:0:{}s:9:"host_vars";a:0:{}s:3:"fit";i:31;s:14:"patternOutline";s:49:"/admin/config/services/twitter-entity/manual-pull";s:8:"numParts";i:5;}}}}',
    'number_parts' => '5',
  ])
  ->values([
    'name' => 'twitter_entity.twitter_settings_form',
    'path' => '/admin/config/services/twitter-entity',
    'pattern_outline' => '/admin/config/services/twitter-entity',
    'fit' => '15',
    'route' => 'C:31:"Symfony\Component\Routing\Route":1110:{a:9:{s:4:"path";s:37:"/admin/config/services/twitter-entity";s:4:"host";s:0:"";s:8:"defaults";a:3:{s:5:"_form";s:47:"\Drupal\twitter_entity\Form\TwitterSettingsForm";s:6:"_title";s:23:"Twitter entity settings";s:12:"_description";s:41:"Administer Twitter accounts and API keys.";}s:12:"requirements";a:1:{s:11:"_permission";s:27:"administer twitter entities";}s:7:"options";a:4:{s:14:"compiler_class";s:33:"Drupal\Core\Routing\RouteCompiler";s:12:"_admin_route";b:1;s:4:"utf8";b:1;s:14:"_access_checks";a:1:{i:0;s:23:"access_check.permission";}}s:7:"schemes";a:0:{}s:7:"methods";a:2:{i:0;s:3:"GET";i:1;s:4:"POST";}s:9:"condition";s:0:"";s:8:"compiled";C:33:"Drupal\Core\Routing\CompiledRoute":410:{a:11:{s:4:"vars";a:0:{}s:11:"path_prefix";s:0:"";s:10:"path_regex";s:45:"#^/admin/config/services/twitter\-entity$#sDu";s:11:"path_tokens";a:1:{i:0;a:2:{i:0;s:4:"text";i:1;s:37:"/admin/config/services/twitter-entity";}}s:9:"path_vars";a:0:{}s:10:"host_regex";N;s:11:"host_tokens";a:0:{}s:9:"host_vars";a:0:{}s:3:"fit";i:15;s:14:"patternOutline";s:37:"/admin/config/services/twitter-entity";s:8:"numParts";i:4;}}}}',
    'number_parts' => '4',
  ])
  ->execute();

$connection->schema()->createTable('twitter_entity', [
  'fields' => [
    'id' => [
      'type' => 'serial',
      'not null' => TRUE,
      'size' => 'normal',
      'unsigned' => TRUE,
    ],
    'uuid' => [
      'type' => 'varchar_ascii',
      'not null' => TRUE,
      'length' => '128',
    ],
    'langcode' => [
      'type' => 'varchar_ascii',
      'not null' => TRUE,
      'length' => '12',
    ],
    'status' => [
      'type' => 'int',
      'not null' => TRUE,
      'size' => 'tiny',
    ],
    'created' => [
      'type' => 'int',
      'not null' => TRUE,
      'size' => 'normal',
    ],
    'changed' => [
      'type' => 'int',
      'not null' => FALSE,
      'size' => 'normal',
    ],
    'tweet_id' => [
      'type' => 'varchar',
      'not null' => FALSE,
      'length' => '255',
    ],
    'tweet_media' => [
      'type' => 'varchar',
      'not null' => FALSE,
      'length' => '2000',
    ],
    'tweet_text__value' => [
      'type' => 'text',
      'not null' => FALSE,
      'size' => 'big',
    ],
    'tweet_text__format' => [
      'type' => 'varchar_ascii',
      'not null' => FALSE,
      'length' => '255',
    ],
    'twitter_user' => [
      'type' => 'varchar',
      'not null' => FALSE,
      'length' => '255',
    ],
    'full_response' => [
      'type' => 'text',
      'not null' => FALSE,
      'size' => 'big',
    ],
  ],
  'primary key' => [
    'id',
  ],
  'unique keys' => [
    'twitter_entity_field__uuid__value' => [
      'uuid',
    ],
  ],
  'indexes' => [
    'twitter_entity_field__tweet_text__format' => [
      'tweet_text__format',
    ],
  ],
  'mysql_character_set' => 'utf8mb4',
]);
