<?php

namespace Drupal\Tests\twitter_entity\Functional\Update;

use Drupal\FunctionalTests\Update\UpdatePathTestBase;

/**
 * Tests the upgrade path for next execution timestamp to the State API.
 *
 * @group twitter_entity
 * @group legacy
 */
class NextExecutionStateUpdateTest extends UpdatePathTestBase {

  /**
   * {@inheritdoc}
   */
  protected function setDatabaseDumpFiles() {
    $this->databaseDumpFiles = [
      DRUPAL_ROOT . '/core/modules/system/tests/fixtures/update/drupal-8.bare.standard.php.gz',
      __DIR__ . '/../../../fixtures/update/test_next_execution_state.php',
    ];
  }

  /**
   * Tests that the 'next_execution' config setting is moved to state.
   */
  public function testNextExecutionStateUpdate() {
    $next_execution = $this->config('twitter_entity.settings')->get('next_execution');
    $this->assertEquals(2500, $next_execution, 'Config value next_execution exists.');

    $this->runUpdates();

    $config = $this->config('twitter_entity.settings')->get();
    $this->assertArrayNotHasKey('next_execution', $config, 'Module config no longer has next_execution config entry.');

    $next_execution_state = $this->container
      ->get('state')
      ->get('twitter_entity.next_execution');
    $this->assertEquals(2500, $next_execution_state, 'Existing next_execution value transferred to state.');
  }

}
