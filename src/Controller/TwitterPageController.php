<?php

namespace Drupal\twitter_entity\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\twitter_entity\TwitterEntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller for twitter_entity module pages.
 */
class TwitterPageController extends ControllerBase {

  /**
   * Constructs a TwitterPageController object.
   *
   * @param \Drupal\twitter_entity\TwitterEntityManager $twitter_manager
   *   Twitter manager service.
   */
  public function __construct(TwitterEntityManager $twitter_manager) {
    $this->twitterManager = $twitter_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('twitter_entity.twitter_manager')
    );
  }

  /**
   * Builds the manual pull page.
   *
   * @return array
   *   Page with information about pulled Tweets.
   */
  public function manualPull() {
    $pull_status = $this->twitterManager->pull();

    if (is_array($pull_status) && !empty($pull_status['error'])) {
      return [
        '#prefix' => '<p>',
        '#suffix' => '</p>',
        '#markup' => $pull_status['error'],
      ];
    }

    return [
      '#prefix' => '<p>',
      '#suffix' => '</p>',
      '#markup' => $pull_status,
    ];
  }

}
