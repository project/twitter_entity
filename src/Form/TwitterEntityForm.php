<?php

namespace Drupal\twitter_entity\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Twitter entity edit forms.
 */
class TwitterEntityForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $status = parent::save($form, $form_state);

    $args = ['%label' => $this->entity->label()];
    $message_text = $status === SAVED_NEW
      ? $this->t('Created the %label Twitter entity.', $args)
      : $this->t('Saved the %label Twitter entity.', $args);
    $this->messenger()->addStatus($message_text);

    $form_state->setRedirect('entity.twitter_entity.canonical', [
      'twitter_entity' => $this->entity->id(),
    ]);

    return $status;
  }

}
