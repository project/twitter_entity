<?php

namespace Drupal\twitter_entity\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Twitter entity entities.
 */
class TwitterEntityDeleteForm extends ContentEntityDeleteForm {

}
