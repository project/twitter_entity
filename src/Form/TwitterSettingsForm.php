<?php

namespace Drupal\twitter_entity\Form;

use Abraham\TwitterOAuth\TwitterOAuth;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Twitter Entity settings for the site.
 */
class TwitterSettingsForm extends ConfigFormBase {

  /**
   * The state key/value store.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Constructs a TwitterSettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state key/value store.
   */
  public function __construct(ConfigFactoryInterface $config_factory, StateInterface $state) {
    parent::__construct($config_factory);
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('state')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'twitter_entity.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'twitter_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('twitter_entity.settings');

    $twitter_app_url = Url::fromUri('https://dev.twitter.com/apps', [
      'attributes' => [
        'target' => '_blank',
      ],
    ]);
    $twitter_app_url = Link::fromTextAndUrl('https://dev.twitter.com/apps', $twitter_app_url);

    $markup = $this->t('In order to finish module setup create twitter app @twitter_app_url',
      ['@twitter_app_url' => $twitter_app_url->toString()]
    );

    $form['twitter_app'] = [
      '#prefix' => '<p>',
      '#suffix' => '</p>',
      '#markup' => $markup,
    ];

    $form['twitter_user_names'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Twitter user name/names'),
      '#description' => $this->t('A list of Twitter accounts. Enter one or more user name on each line.'),
      '#required' => TRUE,
      '#default_value' => $config->get('twitter_user_names'),
    ];

    $form['tweets_number_per_request'] = [
      '#type' => 'number',
      '#title' => $this->t('Tweets number to pull'),
      '#description' => $this->t('Tweets number to pull per request for each user defined above.'),
      '#required' => TRUE,
      '#default_value' => $config->get('tweets_number_per_request'),
      '#attributes' => [
        'min' => 1,
        'max' => 300,
      ],
    ];

    $form['fetch_interval'] = [
      '#type' => 'select',
      '#title' => $this->t('Fetch interval'),
      '#description' => $this->t('How often check if there are some new tweets.'),
      '#default_value' => $config->get('fetch_interval'),
      '#options' => [
        60 => $this->t('1 minute'),
        300 => $this->t('5 minutes'),
        3600 => $this->t('1 hour'),
        86400 => $this->t('1 day'),
      ],
    ];

    $next_execution = $this->state->get('twitter_entity.next_execution');
    if (!empty($next_execution)) {
      $fetchDate = date('d-m-Y G:i', $next_execution);
      $form['interval_text'] = [
        '#prefix' => '<p>',
        '#suffix' => '</p>',
        '#markup' => $this->t('Next fetch on @fetchDate',
          ['@fetchDate' => $fetchDate]
        ),
      ];
    }

    // Manual pull link.
    $manual_pull = Link::createFromRoute($this->t('Manual pull'), 'twitter_entity.manual_pull');

    $form['manual_pull'] = [
      '#prefix' => '<p>',
      '#suffix' => '</p>',
      '#markup' => $manual_pull->toString(),
    ];

    $form['keys_tokens'] = [
      '#type' => 'details',
      '#title' => $this->t('Twitter Keys and Access Tokens'),
      '#open' => TRUE,
      '#tree' => TRUE,
    ];

    $form['keys_tokens']['consumer_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Consumer Key (API Key)'),
      '#required' => TRUE,
      '#default_value' => $config->get('consumer_key'),

    ];

    $form['keys_tokens']['consumer_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Consumer Secret (API Secret)'),
      '#required' => TRUE,
      '#default_value' => $config->get('consumer_secret'),
    ];

    $form['keys_tokens']['oauth_access_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Access Token'),
      '#required' => TRUE,
      '#default_value' => $config->get('oauth_access_token'),
    ];

    $form['keys_tokens']['oauth_access_token_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Access Token Secret'),
      '#required' => TRUE,
      '#default_value' => $config->get('oauth_access_token_secret'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $values = $form_state->getValues();
    $keys = $values['keys_tokens'];

    if (!is_numeric($values['tweets_number_per_request']) || $values['tweets_number_per_request'] <= 0) {
      $form_state->setErrorByName('tweets_number_per_request', $this->t('Must be a number greater then 0.'));
    }

    // Init connection with twitter API.
    $connection = new TwitterOAuth(
      $keys['consumer_key'],
      $keys['consumer_secret'],
      $keys['oauth_access_token'],
      $keys['oauth_access_token_secret']
    );

    // Check if provided API information are correct.
    $account_verify = $connection->get("account/verify_credentials");
    if (isset($account_verify->errors)) {
      $form_state->setErrorByName('keys_tokens', $this->t('Incorrect API keys information.'));
    }
    else {
      // Validate user names only if API keys are valid.
      // Check if all provided twitter user names exist on twitter.
      $twitter_usernames = explode(PHP_EOL, $form_state->getValue('twitter_user_names'));
      $incorrect_twitter_usernames = [];

      foreach ($twitter_usernames as $username) {
        $twitter_user = $connection->get("users/lookup", ["screen_name" => trim($username)]);
        if (isset($twitter_user->errors)) {
          array_push($incorrect_twitter_usernames, trim($username));
        }
      }

      // If there incorrect user names.
      if (!empty($incorrect_twitter_usernames)) {
        $form_state->setErrorByName('twitter_user_names',
          $this->t('Following twitter user names are incorrect: @users',
            ['@users' => implode(', ', $incorrect_twitter_usernames)]
          ));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $values = $form_state->getValues();
    $config = $this->config('twitter_entity.settings');
    $config->set('twitter_user_names', $values['twitter_user_names']);
    $config->set('tweets_number_per_request', $values['tweets_number_per_request']);
    $config->set('fetch_interval', $values['fetch_interval']);

    $config->set('consumer_key', $values['keys_tokens']['consumer_key']);
    $config->set('consumer_secret', $values['keys_tokens']['consumer_secret']);
    $config->set('oauth_access_token', $values['keys_tokens']['oauth_access_token']);
    $config->set('oauth_access_token_secret', $values['keys_tokens']['oauth_access_token_secret']);
    $config->save();
  }

}
